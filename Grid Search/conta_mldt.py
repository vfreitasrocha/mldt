import os

def count_mldt(dirname="./winlogs"):
  filenames = []
  for file in os.listdir(dirname):
      if file.endswith(".out"):
          filenames.append(os.path.join(dirname, file))
  combined = 0
  individual = 0
  for fn in filenames:
    file = open(fn, "r")
    content = file.read()
    n_lines = content.count('\n')
    if n_lines > 2 and n_lines < 5:
      #print(content)
      if 'COMBINED' in content:
        combined = combined + 1
      else:
	      individual = individual + 1
  return combined, individual

c,i = count_mldt('./outm')

print("COMBINED %s\nINDIVIDUAL %s" %(c/(c+i), i/(c+i)))