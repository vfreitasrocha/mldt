#!/bin/bash

#data=(emotions enron genbase image medical reuters scene slashdot yeast mediamill bibtex)

#a=$((($1) / 11))
#b=$((($1) % 11))
#c=$((($a) % 8))
#d=$((($a) % 8))
#e=$((($d) % 8))
#f=$((($d) / 10))

cont=0
for ax in `seq 0 10`
do
for bx in `seq 0 8`
do
for cx in `seq 0 8`
do
for dx in `seq 0 10`
do
	if [ "$cont" = "$1" ]
	then
		a=$ax
		b=$bx
		c=$cx
		d=$dx
	fi
	cont=$((cont+1))
done
done
done
done

data=(emotions reuters image slashdot yeast scene bibtex flags Corel5k Corel16k001 birds)
es=(2 4 8 16 32 64 128 256)
method=(1 2 3 4 5 6 7 8)
nfold=10
cfold=$d

#data=(Arts1 bibtex birds bookmarks Business1 CAL500 Computers1 Corel5k Corel16k001 delicious Education1 emotions Entertainment1 eurlex-dc-leaves flags Health1 image medical rcv1subset1 Recreation1 Reference1 reuters scene Science1 Social1 Society1 tmc2007 yeast)

db=${data[a]}
en=${es[b]}
m=${method[c]}
dp=data/$d #datapath
#dp=data/
exprdir=/home/vrocha/MLDTPaper2017/resultsDT
execjar=../dist/mulan150proj
jvopts=-Xmx3G
resultsdir=resultsDT
seed=12

cd $exprdir ; timeout 5h java $jvopts -jar $execjar.jar $db $en $m $nfold $cfold
