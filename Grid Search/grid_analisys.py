import os
import numpy as np
import pandas as pd


def create_unified_file(output_file='grid.csv', dirname="./winlogs", convert_comma=False):
  filenames = []
  for file in os.listdir(dirname):
      if file.endswith(".out"):
          filenames.append(os.path.join(dirname, file))
          

  output = open(output_file, 'w')
  
  header = ['basename','method','en_size','fold','ExampleBasedAccuracy',
           'ExampleBasedPrecision',
           'ExampleBasedRecall',
           'HammingLoss',
           'SubsetAccuracy',
           'ExampleBasedFMeasure']
           
  output.write(';'.join(header) + '\n')
  
  for fn in filenames:
    file = open(fn, "r")
    content = file.read()
    n_lines = content.count('\n')
    if n_lines > 2 and n_lines < 5:
      content = content.replace('\n',';')
      if(convert_comma):
        output.write(content[:-2].replace(",", ".") + '\n' )
      else:
        output.write(content[:-2] + '\n')
      
  output.close()
  
def combine_results(input_files=['grid.csv'], output_file='grid_mean.csv'):
  df=pd.read_csv(input_files[0], sep=';',header=0)
  for i in range(1,len(input_files)):
      input_file = input_files[i]
      df_i = pd.read_csv(input_file, sep=';', header=0)
      df.update(df_i, raise_conflict=False)
  
  print(df.columns)
  basenames = np.unique(df.basename)
  methods = np.unique(df.method)
  sizes = np.unique(df.en_size)

  output = open(output_file, 'w')
  header = ['basename','method','en_size','ExampleBasedAccuracy',
           'ExampleBasedPrecision',
           'ExampleBasedRecall',
           'HammingLoss',
           'SubsetAccuracy',
           'ExampleBasedFMeasure']
           
  output.write(','.join(header) + '\n')
  
  
  print(df.head())
  medias = []
  for b in basenames:
    for m in methods:
      for s in sizes:
        selecionados = df[(df.basename == b) & (df.method == m) & (df.en_size == s)]
        medias.append([b,m,s,
                       np.mean(selecionados.ExampleBasedAccuracy),
                       np.mean(selecionados.ExampleBasedPrecision),
                       np.mean(selecionados.ExampleBasedRecall),
                       np.mean(selecionados.HammingLoss),
                       np.mean(selecionados.SubsetAccuracy),
                       np.mean(selecionados.ExampleBasedFMeasure)])
  
  for media in medias:
    output.write(str(media)[1:-1] + '\n')

create_unified_file(output_file='grid_c16k.csv', dirname="./c16klogs", convert_comma=True)
combine_results(input_files=['grid_c16k.csv'], output_file='grid_mean_c16k.csv')

#combine_results(input_files=['grid.csv', 'grid02.csv', 'grid03.csv'], output_file='grid_mean_03.csv')

#create_unified_file(output_file='grid03.csv', dirname="./winlogs_03")